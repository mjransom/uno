import pygame
from os import path
import random
import uno_values
from time import sleep
from threading import Thread
from threading import Semaphore
import math

class RectBar(pygame.sprite.Sprite):
    def __init__(self, game):
        pygame.sprite.Sprite.__init__(self)
        self.game = game
        #self.image = pygame.Surface((200, 40))
        #self.image.set_colorkey(uno_values.BLACK)

        self.rect = pygame.Rect(300, 1, 1200, 50)

        self.rect.x = 300
        self.rect.y = 1
        self.rect.width = 1200
        self.rect.height = 50
        pygame.draw.rect(self.game.screen, uno_values.WHITE, self.rect, 2)

    #outline = pygame.draw.rect(self.screen, uno_values.WHITE, outline_rect, 2)

class ColorPicker(pygame.sprite.Sprite):
    def __init__(self, color):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 40))
        self.image.fill(color)
        self.image.set_colorkey(uno_values.BLACK)

        self.rect = self.image.get_rect()
        print(self.rect)
        self.color_value = ""
        self.rect.x = 300
        self.rect.y = 5


    def update(self, *args):
        pass


class Card(pygame.sprite.Sprite):
    def __init__(self, value, color, card_id, game):
        pygame.sprite.Sprite.__init__(self)
        self.game = game
        self.image_path = path.join(path.dirname(__file__), "img/cards")
        self.color = color
        self.back_img = self.image_path+"/card_back.png"
        self.value = value
        self.orientation = "back"
        self.card_id = card_id
        self.posx = 0
        self.posy = 0
        self.assign_front_image()
        self.rect = self.image.get_rect()

        self.timer = pygame.time.get_ticks()
        self.image.set_colorkey(uno_values.BLACK)  # Sets black to transparent.
        self.fast_speed = 100
        self.slow_speed = 1
        self.layer = ""
        self._image = ""
        self.size = "large"

    def find_multiplier(self,start,dest):
        distance = dest - start
        # print(f"distance = {distance}, start = {start} dest = {dest}")

        result = distance / self.fast_speed
        # print(f"result = {result}")

        if result == 0:
            return 0
        elif result > 1 : #or result < -1: # and distance > 0:
            return 1

        elif result < -1:# or result > -1: # and distance < 0:
            return -1

        # elif distance > 0:
        #     return result

        else:
            return result # * -1

    def update(self):
        now = pygame.time.get_ticks()
        if self.orientation == "front":  # Only update cards positions we want to move.  No need to
                                        # update cards sitting on the deck

            if self.rect.x != self.posx or self.rect.y != self.posy:
                #print(self.fast_speed * self.find_multiplier(self.rect.x, self.posx))
                self.rect.x += self.fast_speed * self.find_multiplier(self.rect.x, self.posx)
                self.rect.y += self.fast_speed * self.find_multiplier(self.rect.y, self.posy)

    @property
    def image(self):
        if self.orientation == "front" and self.size == "large":
            #org_image = pygame.image.load(self.front_image).convert()
            #image = org_image.copy()
            self._image = pygame.image.load(self.front_image).convert()
            #self.rect = self._image.get_rect()
            return self._image

        elif self.orientation == "front" and self.size == "small":
            org_image = pygame.image.load(self.front_image).convert()
            image = org_image.copy()
            size = image.get_size()
            width = int(size[0] / 2)
            height = int(size[1] / 2)

            self._image = pygame.transform.scale(org_image, (width, height))

            #self.rect = self.front_image.get_rect()
            return self._image
        else:
            self._image = pygame.image.load(self.back_img).convert()
            return self._image
    @image.setter
    def image(self, value):
        self._image = value

    def flip_card(self):
        if self.orientation == "front":
            self.orientation = "back"
        else:
            self.orientation = "front"

    def assign_front_image(self):
        special_card_types = ["picker", "reverse", "skip"]
        # Assign cards 0 - 9
        if self.value >=0 and self.value <=9:
            self.front_image = self.image_path+"/"+self.color+"_"+str(self.value)+".png"
        # Assign Skip, Reverse, and Draw 2
        elif self.value >=10 and self.value <= 12:
            self.front_image = self.image_path+"/"+self.color+"_"+special_card_types[self.value-10]+".png"

        # Assign Wild Cards
        elif self.value == 13:
            self.front_image = self.image_path+"/"+self.color + ".png"

    def play_sound(self):
        pass


class Player(pygame.sprite.Sprite):
    def __init__(self, game):
        pygame.sprite.Sprite.__init__(self)

        self.game = game
        self._cards = []
        self.points = 0
        self.name = ""
        self.is_computer = False
        self.player_id = ""
        self.card_group = pygame.sprite.Group()

    @property
    def cards(self):
        return self._cards

    def add_card(self, new_card):
        if len(self._cards) < 9:
            for card in self._cards:
                card.size = "large"
            new_card.size = "large"
            self._cards.append(new_card)

        elif len(self._cards) >= 9:
            for card in self._cards:
                card.size = "small"
            new_card.size = "small"
            self._cards.append(new_card)

    def remove_card(self, old_card):
        if len(self._cards) < 9:
            for card in self._cards:
                card.size = "large"
            old_card.size = "large"
            self._cards.remove(old_card)

        elif len(self._cards) >= 9:

            for card in self._cards:
                card.size = "small"
            old_card.size = "small"
            self._cards.remove(old_card)


class Game(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        pygame.init()
        pygame.mixer.init()
        self.width = 1400
        self.height = 700
        self.players = []
        self.game_direction = "forward"
        self.fps = 600
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("Uno!!")
        self.clock = pygame.time.Clock()
        self.cards = []
        self.deck = []
        self.discard_deck = []
        self.player_turn = ""
        self.running = True
        self.number_of_decks = 1
        self.current_color = ""
        self.all_sprites = pygame.sprite.Group()
        self.bar_group = pygame.sprite.Group()
        self.limit = pygame.time.get_ticks()
        self.cards_dealt = False
        self.color_pickers = []


    def process_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_d and not self.cards_dealt:
                    self.cards_dealt = True
                    t = Thread(target=self.deal, daemon=False)
                    t.start()
                if event.key == pygame.K_n:
                    self.running = False
                    main()

                if event.key == pygame.K_q:
                    self.running = False
                    exit(0)

            if event.type == pygame.MOUSEBUTTONDOWN:
                for card in self.players[0].cards:
                    if card.rect.collidepoint(event.pos):
                        self.card_in_hand_clicked(card)
                for deck_card in self.deck:
                    if deck_card.rect.collidepoint(event.pos):
                        self.pick_card(self.deck[len(self.deck)-1])
                        break
                for color in self.color_pickers:
                    if color.rect.collidepoint(event.pos):
                        print(color.color_value)
                        break

    def reorder_hand(self):
        pos = 0
        number_of_cards = len(self.players[0].cards)
        if number_of_cards < 9:
            for card in self.players[0].cards:
                card.size = "large"
            spacing = 130
        elif number_of_cards >= 9:
            spacing = 75
            for card in self.players[0].cards:
                card.size = "small"

        for card in self.players[0].cards:
            card.posx = pos
            card.posy = 500
            pos += spacing
            self.all_sprites.remove(card)
            self.all_sprites.add(card)
            sleep(.1)
            # while card.rect.x != card.posx and card.rect.y != card.posy:
            #     sleep(.1)

    def card_in_hand_clicked(self, card):
        def move_card():
            self.all_sprites.remove(card)
            self.all_sprites.add(card)
            card.posx = 0
            card.posy = 200
            self.discard_deck.append(card)
            self.players[0].remove_card(card)
            card.size = "large"
            t = Thread(target=self.reorder_hand)
            t.start()


        discard_deck_len = len(self.discard_deck)-1
        dis_card = self.discard_deck[discard_deck_len]
        if card.color == self.current_color:
            move_card()

        elif card.value == dis_card.value:
            self.current_color = card.color
            move_card()
        elif card.color == "wild_color_changer":
            #  TODO
            colors = ["red","blue","green","yellow"]
            number = random.randint(0, 3)
            self.current_color = colors[number]
            print(self.current_color)
            move_card()

        elif card.color == "wild_pick_four":
            #  TODO
            colors = ["red","blue","green","yellow"]
            number = random.randint(0, 3)
            self.current_color = colors[number]
            print(self.current_color)
            move_card()

    def pick_card(self, card):
        if self.cards_dealt:

            card.flip_card()
            card.posx = len(self.players[0].cards) * 130
            card.posy = 500
            self.all_sprites.remove(card)
            self.all_sprites.add(card)
            #
            self.deck.pop()
            self.players[0].add_card(card)

            t = Thread(target=self.reorder_hand)
            #self.reorder_hand()
            t.start()
            #t.join()

        else:
            self.deal()

    def display_color_picker(self):
        # TODO Left off with picker.  Next thing is to move it down and draw a line around it.
        colors = [uno_values.RED, uno_values.DARKGREEN, uno_values.BLUE, uno_values.YELLOW]
        color_values = ["red", "green", "blue", "yellow"]
        spacing = 0

        for color, color_value in zip(colors, color_values):
            picker = ColorPicker(color)
            picker.color_value = color_value
            picker.rect.x = 300 + spacing
            self.all_sprites.add(picker)
            self.color_pickers.append(picker)
            self.bar_group.add(picker)
            spacing += 50













    def run(self):
        self.create_deck()
        self.shuffle_deck()
        self.display_color_picker()
        #self.all_sprites.add((self.screen, uno_values.WHITE, pygame.Rect(300, 1, 1200, 50), 10))


        # Game loop, do these things until the game ends
        while self.running:
            self.clock.tick(self.fps)  # Keep the game running at the speed of FPS
            #  Process input events
            self.process_events()




            # Update
            self.all_sprites.update()  # Update all sprites based on their update rules

            # Draw/Render
            self.screen.fill(uno_values.GREEN)
            self.all_sprites.draw(self.screen)  # Draw all sprites on the screen
            outline_rect = pygame.Rect(300, 4, 200, 40)
            pygame.draw.rect(self.screen, uno_values.WHITE, outline_rect, 2)
            pygame.display.flip()  # Flip double buffer into the screen

        pygame.quit()

    def create_player(self, name):
        new_player = Player(self)
        new_player.name = name
        self.players.append(new_player)

    def create_deck(self):
        # Create Color Cards
        card_id = 0
        card_colors = ["green", "red", "yellow", "blue"]
        for card_color in card_colors:
            for card_value in range(0, 13):
                self.deck.append(Card(value=card_value, color=card_color, card_id=card_id, game=self))
                card_id += 1
                self.deck.append(Card(value=card_value, color=card_color, card_id=card_id, game=self))
                card_id += 1

        #  Create Wild Draw 4 Cards
        for card_value in range(0, 4):
            self.deck.append(Card(value=13, color="wild_color_changer", card_id=card_id, game=self))
            card_id += 1
            self.deck.append(Card(value=13, color="wild_pick_four", card_id=card_id, game=self))
            card_id += 1

    def shuffle_deck(self):
        temp_deck = []
        while len(self.deck) > 0:
            number = random.randint(0, len(self.deck)-1)
            temp_deck.append(self.deck.pop(number))

        self.deck = temp_deck
        for card in self.deck:
            self.all_sprites.add(card)

    def deal(self):
        pos = 0
        for player in self.players:
            for x in range(0, 7):
                card = self.deck.pop()
                card.flip_card()
                card.posx = pos
                card.posy = 500
                pos += 130
                self.all_sprites.remove(card)
                self.all_sprites.add(card)
                player.add_card(card)
                card.rect = card.image.get_rect()
                #player.cards.append(card)
                sleep(.25)
                # while card.rect.x != card.posx and card.rect.y != card.posy:
                #     sleep(.5)

        first_card = self.deck.pop()
        first_card.flip_card()
        first_card.posy = 200
        self.discard_deck.append(first_card)
        self.all_sprites.add(first_card)
        self.current_color = first_card.color
        self.cards_dealt = True


def main():
    game = Game()
    game.create_player("Mike")
    game.run()


if __name__ == '__main__':
    main()










